---
title: 網站架設Hexo
date: 2020-03-08 17:13:50
tags:
---
## 加新網頁 ##

hexo n "新網頁名稱"
在_posts/新網頁名稱.md 檔案中更改資料

## 刪除網頁 ##

直接刪除在_posts/新網頁名稱.md 檔案

## 局部測試 ##

hexo g  : 重新產生資料  
hexo s  : 部局在 localhost:4000 port中

## 推送到gitlab ##

可在vs code中或sourceTree工具中執行
git add . (一次加入所有變更跟新增檔案，但不包括刪除的檔案!)
git commit -m “xxx”
git push -u origin master



&ensp;這是退一格 \&ensp;
&emsp;這是退兩格 \&emsp;