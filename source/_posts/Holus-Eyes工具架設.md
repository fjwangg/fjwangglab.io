---
title: Holus Eyes工具架設
date: 2020-03-08 17:59:03
tags:
---

## 工作進程 ##

2020-03-16 11:09:07
了解了，extension point的用法，就是把一堆功能register上去，等register call下來用，最主要的好偵處是call的路徑方便，複雜功能如我們要做的editor時就要用到這功能不然太難了
拿 BranchPolicyManager extends RegistryReader做驗証
        1. policy的主要目的就是把request轉成command
        2. 例如： TopicMovablePolicy才會有policy功能，barnchpolicy是虛的，等下再了解
        3. branchPart是假的一備集合，把功能分到底下的part，例如：topicPart
        4. 


2020-03-15 19:16:36
澄清 addon service 與 handler之間的關係

2020-03-15 14:15:35
假設知道了 part -> deco -> figure 的路徑，下一步研究command的種類

了解了，layout的改變
        1. <property
                name="rendererFactoryUri"
                value="bundleclass://org.xmind.cathy/org.xmind.cathy.internal.renderer.XWorkbenchRendererFactory">
          </property>
        2. 這個參數與 xxxRenderer.java就是來改變原來的系統的MComponents的，這樣就會讓layout改成自己想要的樣子 new model element

2020-03-14 21:13:40
回來原點，再回想一下方式

1. 先試改變圖形
   - 先針對plusminus fingle 來改變，有用的，改變paintFigure(Graphics g)
   - 下一步分析plusminusDecorator，查到decorator的目的就是查資料然後把資料設定到figure身上
   - 再查一下其它的decorator, 
2. 加入元件分析
3. 的

<!-- 2020-03-08 18:03:38
目標： 先執行劃圖能力，用text tile當例子 -->
2020-03-08 23:57:24
分析到了"由BranchStructure來填寫這資料"，明天再追其它structure也有填寫這一比資料喔！
2020-03-09 10:19:58
BranchStructure分析 ：
Topic分析 ：
再一次想辦法進初步layout ：
***

## XMind分析 ##

### LayoutManager ###

#### [AbstractReferencedLayout] ####

1. 功能： layoutManager的底層，有一主要 layout()來佈局figure與childFigure的位置
2. private ReferencedLayoutData layoutData
3. private boolean calculating :
4. private Insets referenceDescription
5. private Rectangle prefClientArea :
6. calculatePreferredSize(IFigure container, int wHint,int hHint): 得到中央figure的四方形+insets(邊框)
7. Rectangle getPreferredClientArea(IFigure container) : 回覆兩種clientArea其中的一種
8. Insets getReferenceDescription(IFigure figure) : 取得邊框資料
9. Insets calculateReferenceDescription(IFigure figure) : 一開始邊框資料的計算方式
10. void layout(IFigure container) : 先由資料來取得平移資料，把各個childFigure平移後再劃圖
11. abstract void fillLayoutData(IFigure container ReferencedLayoutData data) : 由它的子class來寫資料

#### [ReferencedLayoutData] ####

1. private Point reference;
2. private Map<Object, Rectangle> contents
3. private Rectangle clientArea : 用來記錄contents中各物件合起來的最大區域

#### [MindMapPartBase] ####

1. MindMapLayoutBase extends AbstractReferencedLayout

#### [BranchLayout] ####

1. class BranchLayout extends MindMapLayoutBase
2. void fillLayoutData(IFigure container, ReferencedLayoutData data) : 主要完成資料的填寫
   - (IStructure) part.getAdapter(IStructure.class) : 由part取得Structure的資料
   - ((IBranchStructure) sa).fillLayoutData(getBranch(), data) : 由BranchStructure來填寫這資料

#### [TreeStructure] ####

1. TreeStructure extends AbstractBranchStructure
2. void doFillPlusMinus(IBranchPart branch,IPlusMinusPart plusMinus, LayoutInfo info)
3. void doFillSubBranches(IBranchPart branch,List \<IBranchPart> subBranches, LayoutInfo info)

#### [AbstractBranchStructure] ####

1. void fillLayoutData(IBranchPart branch, ReferencedLayoutData data)
2. protected static class LayoutInfo extends ReferencedLayoutData { : 擴展ReferencedLayoutData的功能
        private ReferencedLayoutData delegate;
        private boolean folded;
        private boolean minimized;
        private Rectangle minArea;
        private boolean hasBoundaryTitles;
3. 
4. ok

#### [UnbalancedStructure] ####

1. public class UnbalancedStructure extends ClockwiseRadialStructure
2. Point calcInsertPosition(IBranchPart branch, IBranchPart child, ParentSearchKey key) : 
3. 園

#### [RadialData] ####

1. class RadialData extends BranchStructureData
2. getDefaultMinSumSpacing() : 
3. 
4. 園


DefaultBranchStyleSelector




***

## 工具定義 ##

1. 使用eclipse中的 GEF MVC為開發的底層
2. 加入xmind的關念開發工具
3. ok

***
